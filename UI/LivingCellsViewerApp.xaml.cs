﻿using Avalonia;
using Avalonia.Markup.Xaml;

namespace Ks.LivingCellsPlatform.UI
{
    public class LivingCellsViewerApp : Application
    {

        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

    }
}
