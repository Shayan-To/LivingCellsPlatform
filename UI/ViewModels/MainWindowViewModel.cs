﻿using Avalonia.Media;
using ReactiveUI;

namespace Ks.LivingCellsPlatform.UI.ViewModels
{
    public class MainWindowViewModel<TVector, TBehaviorArgs> : ViewModelBase
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public MainWindowViewModel(Game<TVector, TBehaviorArgs> game)
        {
            this.Game = game;
        }

        public void WindowShownAsync()
        {
            this.GameMediator = new GameUiMediator<TVector, TBehaviorArgs>(this.Game);
            this.RaisePropertyChanged(nameof(this.GameMediator));
            this.RaisePropertyChanged(nameof(this.BoardGeometry));
            this.GameMediator.Start();
        }

        public Geometry BoardGeometry => this.GameMediator?.Game?.CoordinateSystem?.GetBoardGeometry();

        public GameUiMediator<TVector, TBehaviorArgs> GameMediator
        {
            private set;
            get;
        }

        public Game<TVector, TBehaviorArgs> Game
        {
            get;
        }

    }
}
