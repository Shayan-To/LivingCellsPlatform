﻿using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Media;

namespace Ks.LivingCellsPlatform.UI.Controls
{
    public class GameBoard : Control
    {

        static GameBoard()
        {
            AffectsRender(CreaturesProperty);
            AffectsRender(BackgroundProperty);
            BoardGeometryProperty.Changed.AddClassHandler<GameBoard>(o => o.BoardGeometry_Changed);
        }

        public GameBoard()
        {
            this.TranslateTransform = new TranslateTransform();
            this.CurrentTransform = this.TranslateTransform;
            this.EllipseGeometry = new EllipseGeometry() { Transform = this.CurrentTransform };
            this.FillBrush = new SolidColorBrush(Colors.Pink, 0.6);
            this.StrokeBrush = new SolidColorBrush(Colors.DarkGray, 0.6);
            this.StrokePen = new Pen(this.StrokeBrush, 3);
        }

        private void BoardGeometry_Changed(AvaloniaPropertyChangedEventArgs e)
        {
            if (e.NewValue == null)
            {
                this.BoardClip = null;
                return;
            }

            this.BoardClip = ((Geometry) e.NewValue).Clone();
            this.BoardClip.Transform = this.CurrentTransform;

            this.InvalidateVisual();
        }

        public override void Render(DrawingContext context)
        {
            var sz = this.Bounds.Size;
            this.TranslateTransform.X = sz.Width / 2;
            this.TranslateTransform.Y = sz.Height / 2;

            using (this.BoardClip != null ? context.PushGeometryClip(this.BoardClip) : default(DrawingContext.PushedState))
            {
                if (this.Background != null)
                {
                    context.FillRectangle(this.Background, new Rect(sz));
                }
                if (this.Creatures != null)
                {
                    foreach (var c in this.Creatures)
                    {
                        this.EllipseGeometry.Rect = new Rect(c.X - c.Radius, c.Y - c.Radius, c.Radius * 2, c.Radius * 2);
                        context.DrawGeometry(this.FillBrush, this.StrokePen, this.EllipseGeometry);
                    }
                }
            }
        }

        #region Background Property
        private static readonly StyledProperty<IBrush> BackgroundProperty =
#pragma warning disable IDE0009 // Member access should be qualified.
            AvaloniaProperty.Register<GameBoard, IBrush>(nameof(Background));
#pragma warning restore IDE0009 // Member access should be qualified.

        public IBrush Background
        {
            get => this.GetValue(BackgroundProperty);
            set => this.SetValue(BackgroundProperty, value);
        }
        #endregion

        #region Creatures Property
        private static readonly DirectProperty<GameBoard, IEnumerable<(double X, double Y, double Radius)>> CreaturesProperty =
#pragma warning disable IDE0009 // Member access should be qualified.
            AvaloniaProperty.RegisterDirect<GameBoard, IEnumerable<(double X, double Y, double Radius)>>(nameof(Creatures), o => o.Creatures, (o, v) => o.Creatures = v);
#pragma warning restore IDE0009 // Member access should be qualified.

        private IEnumerable<(double X, double Y, double Radius)> _Creatures;

        public IEnumerable<(double X, double Y, double Radius)> Creatures
        {
            get => this._Creatures;
            set => this.SetAndRaise(CreaturesProperty, ref this._Creatures, value);
        }
        #endregion

        #region BoardGeometry Property
        private static readonly DirectProperty<GameBoard, Geometry> BoardGeometryProperty =
#pragma warning disable IDE0009 // Member access should be qualified.
            AvaloniaProperty.RegisterDirect<GameBoard, Geometry>(nameof(BoardGeometry), o => o.BoardGeometry, (o, v) => o.BoardGeometry = v);
#pragma warning restore IDE0009 // Member access should be qualified.

        private Geometry _BoardGeometry;

        public Geometry BoardGeometry
        {
            get => this._BoardGeometry;
            set => this.SetAndRaise(BoardGeometryProperty, ref this._BoardGeometry, value);
        }
        #endregion

        private Geometry BoardClip;
        private readonly TranslateTransform TranslateTransform;
        private readonly Transform CurrentTransform;
        private readonly EllipseGeometry EllipseGeometry;
        private readonly IBrush FillBrush;
        private readonly IBrush StrokeBrush;
        private readonly Pen StrokePen;

    }
}
