﻿using System;
using Avalonia;
using Avalonia.Controls;
using Ks.LivingCellsPlatform.UI.ViewModels;
using Ks.LivingCellsPlatform.UI.Views;

namespace Ks.LivingCellsPlatform.UI
{
    public static class LivingCellsViewer
    {

        public static void StartApp<TVector, TBehaviorArgs, TAppBuilder>(AppBuilderBase<TAppBuilder> appBuilder, Func<Game<TVector, TBehaviorArgs>> gameProvider)
            where TBehaviorArgs : BehaviorArgs<TVector>, new()
            where TAppBuilder : AppBuilderBase<TAppBuilder>, new()
        {
            appBuilder.SetupWithoutStarting();
            var window = new MainWindow();
            var viewModel = new MainWindowViewModel<TVector, TBehaviorArgs>(gameProvider.Invoke());
            window.DataContext = viewModel;

            IDisposable subscription = null;
            subscription = window.GetObservable(MainWindow.IsVisibleProperty).Subscribe(value =>
            {
                if (value)
                {
                    viewModel.WindowShownAsync();
                    subscription.Dispose();
                }
            });

            window.Show();
            appBuilder.Instance.Run(window);
        }

    }
}
