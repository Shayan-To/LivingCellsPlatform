﻿namespace Ks.LivingCellsPlatform
{
    public class SampleGameDynamics<TVector> : GameDynamics<TVector, BehaviorArgs<TVector>>
    {

        public override void PreFrameOperations()
        {
        }

        public override void PostFrameOperations()
        {
        }

        public override void CalculateSensoryData(Creature<TVector, BehaviorArgs<TVector>> creature, BehaviorArgs<TVector> e)
        {
        }

        public override void PostProcessBehaviorArgs(Creature<TVector, BehaviorArgs<TVector>> creature, BehaviorArgs<TVector> e)
        {
            var f = e.Force;
            var fLength = this.CoordinateSystem.Length(f);
            if (fLength > 100)
            {
                e.Force = this.CoordinateSystem.Scale(f, 100 / fLength);
            }
        }

        public override TVector CalculateEnvironmentalForce(CreatureStatics<TVector> c)
        {
            var vel = this.CoordinateSystem.Length(c.Velocity);
            return this.CoordinateSystem.Scale(c.Velocity, -vel / 20);
        }

        public override TVector CalculateInteractionForce(CreatureStatics<TVector> c1, CreatureStatics<TVector> c2)
        {
            var subt = this.CoordinateSystem.Subt(c1.Position, c2.Position);
            subt = this.CoordinateSystem.ShortestForm(subt);
            var dist = this.CoordinateSystem.Length(subt) - c1.Size - c2.Size;

            if (dist > 0)
            {
                return this.CoordinateSystem.ZeroVector;
            }

            dist = -dist;
            return this.CoordinateSystem.NewVector(dist * dist * dist * 10, this.CoordinateSystem.Angle(subt));
        }

        public override TVector PostProcessFinalForce(CreatureStatics<TVector> creature, TVector force)
        {
            var fLength = this.CoordinateSystem.Length(force);
            if (fLength > 100)
            {
                return this.CoordinateSystem.Scale(force, 100 / fLength);
            }
            return force;
        }

    }
}
