﻿using System.Collections.Generic;
using Avalonia.Media;

namespace Ks.LivingCellsPlatform
{
    public class InfiniteCartesianCoordinateSystem : CartesianCoordinateSystem
    {

        public override CartesianVector VectorToPoint(CartesianVector v)
        {
            return v;
        }

        public override IEnumerable<CartesianVector> GetStandardForms(CartesianVector point)
        {
            yield return point;
        }

        public override CartesianVector ShortestForm(CartesianVector point)
        {
            return point;
        }

        public override IEnumerable<(CartesianVector Direction, double Distance)> GetHittingWalls(CartesianVector point, double radius)
        {
            yield break;
        }

        public override Geometry GetBoardGeometry()
        {
            return null;
        }

    }
}
