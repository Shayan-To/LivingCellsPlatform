﻿using System;

namespace Ks.LivingCellsPlatform
{
    public abstract class CartesianCoordinateSystem : CoordinateSystem<CartesianVector>
    {

        public override CartesianVector Add(CartesianVector v1, CartesianVector v2)
        {
            return new CartesianVector(v1.X + v2.X, v1.Y + v2.Y);
        }

        public override CartesianVector Scale(CartesianVector v, double f)
        {
            return new CartesianVector(v.X * f, v.Y * f);
        }

        public override CartesianVector NewVector(double length, double angle)
        {
            return new CartesianVector(length * Math.Cos(angle), length * Math.Sin(angle));
        }

        public override CartesianVector Subt(CartesianVector v1, CartesianVector v2)
        {
            return new CartesianVector(v1.X - v2.X, v1.Y - v2.Y);
        }

        public override CartesianVector Negate(CartesianVector v)
        {
            return new CartesianVector(-v.X, -v.Y);
        }

        public override double LengthSquared(CartesianVector v)
        {
            return v.X * v.X + v.Y * v.Y;
        }

        public override double Angle(CartesianVector v)
        {
            return Math.Atan2(v.Y, v.X);
        }

        public override (double X, double Y) GetCoordinatesForDisplay(CartesianVector point)
        {
            return (point.X, point.Y);
        }

    }
}
