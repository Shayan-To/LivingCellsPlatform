﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ks.LivingCellsPlatform
{
    public class AtOriginCreaturePlacer<TVector, TBehaviorArgs> : CreaturePlacer<TVector, TBehaviorArgs>
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public override void PlaceCreatures(IEnumerable<CreatureType<TVector, TBehaviorArgs>> creatureTypes)
        {
            foreach (var ct in creatureTypes)
            {
                for (var i = 0; i < this.CountOfEach; i++)
                {
                    var c = this.Game.AddCreature(ct);
                    c.Statics.Size = 20;
                }
            }
        }

        public int CountOfEach
        {
            get;
            set;
        } = 1;

    }
}
