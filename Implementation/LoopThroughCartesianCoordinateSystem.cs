﻿using System.Collections.Generic;
using Avalonia.Media;

namespace Ks.LivingCellsPlatform
{
    public class LoopThroughCartesianCoordinateSystem : CartesianCoordinateSystem
    {

        public LoopThroughCartesianCoordinateSystem(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        public override CartesianVector VectorToPoint(CartesianVector v)
        {
            return new CartesianVector((v.X % this.Width + 2 * this.Width) % this.Width,
                                       (v.Y % this.Height + 2 * this.Height) % this.Height);
        }

        public override IEnumerable<CartesianVector> GetStandardForms(CartesianVector point)
        {
            point = this.VectorToPoint(point);
            yield return point;
            yield return new CartesianVector(point.X - this.Width, point.Y);
            yield return new CartesianVector(point.X, point.Y - this.Height);
            yield return new CartesianVector(point.X - this.Width, point.Y - this.Height);
        }

        public override IEnumerable<(CartesianVector Direction, double Distance)> GetHittingWalls(CartesianVector point, double radius)
        {
            yield break;
        }

        public override Geometry GetBoardGeometry()
        {
            if (this.BoardGeometry == null)
            {
                this.BoardGeometry = new RectangleGeometry(new Avalonia.Rect(-this.Width / 2, -this.Height / 2, this.Width, this.Height));
            }
            return this.BoardGeometry;
        }

        public double Width
        {
            get;
        }

        public double Height
        {
            get;
        }

        private RectangleGeometry BoardGeometry;

    }
}
