﻿namespace Ks.LivingCellsPlatform
{
    public class CreatureStatics<TVector>
    {

        public CreatureStatics()
        {
        }

        public CreatureStatics(TVector position, TVector velocity, double size)
        {
            this.Set(position, velocity, size);
        }

        public void Set(TVector position, TVector velocity, double size)
        {
            this.Position = position;
            this.Velocity = velocity;
            this.Size = size;
        }

        public void Set(CreatureStatics<TVector> other)
        {
            this.Position = other.Position;
            this.Velocity = other.Velocity;
            this.Size = other.Size;
        }

        public TVector Position
        {
            get;
            set;
        }

        public TVector Velocity
        {
            get;
            set;
        }

        public double Size
        {
            get;
            set;
        }

    }
}
