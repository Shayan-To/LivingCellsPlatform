﻿namespace Ks.LivingCellsPlatform
{
    public class Creature<TVector, TBehaviorArgs>
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public Creature(CreatureType<TVector, TBehaviorArgs> type)
        {
            this.Type = type;
        }

        public CreatureStatics<TVector> Statics { get; } = new CreatureStatics<TVector>();

        public CreatureType<TVector, TBehaviorArgs> Type
        {
            get;
        }

    }
}
