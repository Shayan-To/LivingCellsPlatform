﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ks.LivingCellsPlatform
{
    public class StateDictionary
    {

        public T Get<T>(StateKey<T> key)
        {
            return (T) this.Dictionary[key];
        }

        public void Set<T>(StateKey<T> key, T value)
        {
            this.Dictionary[key] = value;
        }

        private readonly Dictionary<StateKey, object> Dictionary = new Dictionary<StateKey, object>();

    }
}
