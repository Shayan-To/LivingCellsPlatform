﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;

namespace Ks.LivingCellsPlatform
{
    public class GameUiMediator<TVector, TBehaviorArgs> : INotifyPropertyChanged
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public GameUiMediator(Game<TVector, TBehaviorArgs> game)
        {
            this.Game = game;
            this.SynchronizationContext = SynchronizationContext.Current;
            this.RunningWaitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
            this.Thread = new Thread(this.RunnerThread) { IsBackground = true };
            this.Thread.Start();

            this.UiFramesPerSecond = 30;
        }

        public void Start()
        {
            //this.CreaturesReadDone = true;
            this.RunningWaitHandle.Set();
            lock (this.Stopwatch)
            {
                this.Stopwatch.Start();
            }
        }

        public void Stop()
        {
            this.RunningWaitHandle.Reset();
            lock (this.Stopwatch)
            {
                this.Stopwatch.Stop();
            }
        }

        private void RunnerThread()
        {
            while (true)
            {
                this.RunningWaitHandle.WaitOne();

                double currentTime;
                lock (this.Stopwatch)
                {
                    currentTime = this.Stopwatch.Elapsed.TotalSeconds;
                }

                var logicalElapsedTime = this.Game.CurrentTime;
                this.Game.AdvanceFrameToTime(currentTime);
                logicalElapsedTime = this.Game.CurrentTime - logicalElapsedTime;

                double elapsedTime;
                lock (this.Stopwatch)
                {
                    elapsedTime = this.Stopwatch.Elapsed.TotalSeconds - currentTime;
                }
                if (elapsedTime > logicalElapsedTime * 1.5 && logicalElapsedTime != 0)
                {
                    Console.WriteLine($"WARNING. Framerate too high. Elapsed time: {elapsedTime} Logical elapsed time: {logicalElapsedTime}");
                }

                //if (this.CreaturesReadDone & currentTime > this.LastCreaturesReadTime + this.UiFrameLength)
                if (currentTime > this.LastUiFrame + this.UiFrameLength)
                {
                    this.LastUiFrame = currentTime;
                    lock (this.LockObject)
                    {
                        var i = 0;
                        foreach (var creature in this.Game.GetAllCreatures())
                        {
                            foreach (var p in this.Game.CoordinateSystem.GetStandardForms(creature.Statics.Position))
                            {
                                var XY = this.Game.CoordinateSystem.GetCoordinatesForDisplay(p);
                                var XYR = (XY.X, XY.Y, creature.Statics.Size);
                                if (i < this._Creatures.Count)
                                {
                                    this._Creatures[i] = XYR;
                                }
                                else
                                {
                                    this._Creatures.Add(XYR);
                                }
                                i++;
                            }
                        }
                        this._Creatures.RemoveRange(i, this._Creatures.Count - i);
                        //this.CreaturesReadDone = false;
                    }
                    this.SynchronizationContext.Post(_ => this.OnPropertyChanged(nameof(this.Creatures)), null);
                }

                Thread.Sleep((int)(this.Game.FrameLength * 1000));
            }
        }

        #region PropertyChanged Event
        protected void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Creatures Property
        private List<(double X, double Y, double Radius)> _Creatures = new List<(double X, double Y, double Radius)>();

        public IEnumerable<(double X, double Y, double Radius)> Creatures
        {
            get
            {
                lock (this.LockObject)
                {
                    for (var i = 0; i < this._Creatures.Count; i++)
                    {
                        yield return this._Creatures[i];
                    }
                    //this.LastCreaturesReadTime = this.Stopwatch.Elapsed.TotalSeconds;
                    //this.CreaturesReadDone = true;
                }
            }
        }
        #endregion

        #region UiFramesPerSecond Property
        private double _UiFramesPerSecond;

        public double UiFramesPerSecond
        {
            get => this._UiFramesPerSecond;
            set
            {
                this._UiFramesPerSecond = value;
                this.UiFrameLength = 1.0 / value;
            }
        }
        #endregion

        #region UiFrameLength Property
        private double _UiFrameLength;

        public double UiFrameLength
        {
            get => this._UiFrameLength;
            private set => this._UiFrameLength = value;
        }
        #endregion

        public Game<TVector, TBehaviorArgs> Game
        {
            get;
        }

        //private double LastCreaturesReadTime;
        //private bool CreaturesReadDone;
        private double LastUiFrame;
        private readonly Stopwatch Stopwatch = new Stopwatch();

        private readonly Thread Thread;
        private readonly SynchronizationContext SynchronizationContext;
        private readonly EventWaitHandle RunningWaitHandle;
        private readonly object LockObject = new object();

    }
}
