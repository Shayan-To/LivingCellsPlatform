﻿using System;
using System.Collections.Generic;

namespace Ks.LivingCellsPlatform
{
    public abstract class CreatureStore<TVector, TBehaviorArgs>
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public abstract IEnumerable<Creature<TVector, TBehaviorArgs>> GetAllCreatures();
        public abstract IEnumerable<Creature<TVector, TBehaviorArgs>> GetCreaturesHittingCircle(TVector center, double radius);

        /// <summary>This method can only be used from PreFrameOperations or PostFrameOperations methods.</summary>
        public abstract Creature<TVector, TBehaviorArgs> AddCreature(CreatureType<TVector, TBehaviorArgs> type);
        /// <summary>This method can only be used from PreFrameOperations or PostFrameOperations methods.</summary>
        public abstract void RemoveCreature(Creature<TVector, TBehaviorArgs> creature);

        protected void VerifyAddRemoveCreaturesAllowed()
        {
            if (!this.AddRemoveCreaturesAllowed)
            {
                throw new InvalidOperationException("Cannot add or remove creatures.");
            }
        }

        protected bool AddRemoveCreaturesAllowed = true;

    }
}
