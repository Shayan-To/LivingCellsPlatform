﻿using System;
using System.Collections.Generic;

namespace Ks.LivingCellsPlatform
{
    public class GameHandler
    {

        public static GameHandler<TVector, TBehaviorArgs> Create<TVector, TBehaviorArgs>(CoordinateSystem<TVector> coordinateSystem, GameDynamics<TVector, TBehaviorArgs> gameDynamics, CreaturePlacer<TVector, TBehaviorArgs> creaturePlacer, Random random) where TBehaviorArgs : BehaviorArgs<TVector>, new()
        {
            return Create(new Game<TVector, TBehaviorArgs>(coordinateSystem, gameDynamics), creaturePlacer, random);
        }

        public static GameHandler<TVector, TBehaviorArgs> Create<TVector, TBehaviorArgs>(CoordinateSystem<TVector> coordinateSystem, GameDynamics<TVector, TBehaviorArgs> gameDynamics, CreaturePlacer<TVector, TBehaviorArgs> creaturePlacer) where TBehaviorArgs : BehaviorArgs<TVector>, new()
        {
            return Create(new Game<TVector, TBehaviorArgs>(coordinateSystem, gameDynamics), creaturePlacer);
        }

        public static GameHandler<TVector, TBehaviorArgs> Create<TVector, TBehaviorArgs>(Game<TVector, TBehaviorArgs> game, CreaturePlacer<TVector, TBehaviorArgs> creaturePlacer, Random random) where TBehaviorArgs : BehaviorArgs<TVector>, new()
        {
            return new GameHandler<TVector, TBehaviorArgs>(game, creaturePlacer, random);
        }

        public static GameHandler<TVector, TBehaviorArgs> Create<TVector, TBehaviorArgs>(Game<TVector, TBehaviorArgs> game, CreaturePlacer<TVector, TBehaviorArgs> creaturePlacer) where TBehaviorArgs : BehaviorArgs<TVector>, new()
        {
            return new GameHandler<TVector, TBehaviorArgs>(game, creaturePlacer);
        }

    }

    public class GameHandler<TVector, TBehaviorArgs>
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public GameHandler(Game<TVector, TBehaviorArgs> game, CreaturePlacer<TVector, TBehaviorArgs> creaturePlacer, Random random)
        {
            this.Game = game;
            this.CreaturePlacer = creaturePlacer;
            this.Random = random;

            this.CreaturePlacer.Game = this.Game;
        }

        public GameHandler(Game<TVector, TBehaviorArgs> game, CreaturePlacer<TVector, TBehaviorArgs> creaturePlacer) : this(game, creaturePlacer, new Random())
        {
        }

        public CreatureType<TVector, TBehaviorArgs> AddCreatureType(Action<TBehaviorArgs> behaviorInitializeDelegate, Action<TBehaviorArgs> behaviorDelegate, Func<bool> needsSensoryDataDelegate)
        {
            var ctype = new DelegateCreatureType<TVector, TBehaviorArgs>(behaviorInitializeDelegate, behaviorDelegate, needsSensoryDataDelegate);
            this.CreatureTypes.Add(ctype);
            return ctype;
        }

        public CreatureType<TVector, TBehaviorArgs> AddCreatureType(Action<TBehaviorArgs> behaviorInitializeDelegate, Action<TBehaviorArgs> behaviorDelegate)
        {
            return this.AddCreatureType(behaviorInitializeDelegate, behaviorDelegate, () => true);
        }

        public void FinishInitialization()
        {
            this.CreaturePlacer.PlaceCreatures(this.CreatureTypes);
        }

        public StateKey<T> NewStateKey<T>()
        {
            return new StateKey<T>();
        }

        public Game<TVector, TBehaviorArgs> Game
        {
            get;
        }

        public CreaturePlacer<TVector, TBehaviorArgs> CreaturePlacer
        {
            get;
        }

        public Random Random
        {
            get;
        }

        private readonly List<CreatureType<TVector, TBehaviorArgs>> CreatureTypes = new List<CreatureType<TVector, TBehaviorArgs>>();

    }
}
