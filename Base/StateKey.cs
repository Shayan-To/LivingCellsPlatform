﻿namespace Ks.LivingCellsPlatform
{
    public class StateKey
    {

    }

    public class StateKey<T> : StateKey
    {

        public T this[StateDictionary dic]
        {
            get => dic.Get(this);
            set => dic.Set(this, value);
        }

    }
}
