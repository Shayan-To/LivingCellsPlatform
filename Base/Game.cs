﻿using System;
using System.Collections.Generic;

namespace Ks.LivingCellsPlatform
{
    public class Game<TVector, TBehaviorArgs> : CreatureStore<TVector, TBehaviorArgs>
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public Game(CoordinateSystem<TVector> coordinateSystem, GameDynamics<TVector, TBehaviorArgs> gameDynamics)
        {
            this.CoordinateSystem = coordinateSystem;
            this.GameDynamics = gameDynamics;
            this.FramesPerSecond = 60; // This will set the settings to the game dynamics object.
        }

        public override Creature<TVector, TBehaviorArgs> AddCreature(CreatureType<TVector, TBehaviorArgs> type)
        {
            this.VerifyAddRemoveCreaturesAllowed();
            var creature = new Creature<TVector, TBehaviorArgs>(type);
            var behaviorArgs = new TBehaviorArgs();

            type.BehaviorInitialize(behaviorArgs);

            this.Creatures.Add(creature);
            this.BehaviorArgs.Add(behaviorArgs);

            return creature;
        }

        public override void RemoveCreature(Creature<TVector, TBehaviorArgs> creature)
        {
            this.VerifyAddRemoveCreaturesAllowed();

            var i = this.Creatures.IndexOf(creature);
            if (i < 0)
            {
                throw new InvalidOperationException("Creature does not exist to be removed.");
            }

            this.Creatures.RemoveAt(i);
            this.BehaviorArgs.RemoveAt(i);
        }

        public override IEnumerable<Creature<TVector, TBehaviorArgs>> GetAllCreatures()
        {
            for (var i = 0; i < this.Creatures.Count; i++)
            {
                yield return this.Creatures[i];
            }
        }

        public override IEnumerable<Creature<TVector, TBehaviorArgs>> GetCreaturesHittingCircle(TVector center, double radius)
        {
            for (var i = 0; i < this.Creatures.Count; i++)
            {
                var creature = this.Creatures[i];
                if (this.CoordinateSystem.Dist(center, creature.Statics.Position) <= radius + creature.Statics.Size)
                {
                    yield return creature;
                }
            }
        }

        public void AdvanceOneFrame()
        {
            this.GameDynamics.PreFrameOperations();

            this.AddRemoveCreaturesAllowed = false;

            for (var i = 0; i < this.Creatures.Count; i++)
            {
                var c = this.Creatures[i];
                var e = this.BehaviorArgs[i];
                e.Reset(c.Statics, this.CurrentTime);
                var version = e.Version;

                if (c.Type.NeedsSensoryData)
                {
                    this.GameDynamics.CalculateSensoryData(c, e);
                    if (e.Version != version)
                    {
                        throw new InvalidOperationException("CalculateSensoryData should not reset BehaviorArgs object.");
                    }
                }

                c.Type.Behavior(e);
                if (e.Version != version)
                {
                    throw new InvalidOperationException("Creature Behavior method should not reset BehaviorArgs object.");
                }

                this.GameDynamics.PostProcessBehaviorArgs(c, e);
                if (e.Version != version)
                {
                    throw new InvalidOperationException("PostProcessBehaviorArgs should not reset BehaviorArgs object.");
                }

                var envForce = this.GameDynamics.CalculateEnvironmentalForce(c.Statics);
                e.Force = this.CoordinateSystem.Add(e.Force, envForce);
            }

            for (var i = 0; i < this.Creatures.Count; i++)
            {
                var c1 = this.Creatures[i];
                var e1 = this.BehaviorArgs[i];

                for (var j = i + 1; j < this.Creatures.Count; j++)
                {
                    var c2 = this.Creatures[j];
                    var e2 = this.BehaviorArgs[j];

                    var f = this.GameDynamics.CalculateInteractionForce(c1.Statics, c2.Statics);

                    e1.Force = this.CoordinateSystem.Add(e1.Force, f);
                    e2.Force = this.CoordinateSystem.Subt(e2.Force, f);
                }
            }

            for (var i = 0; i < this.Creatures.Count; i++)
            {
                var c = this.Creatures[i];
                var e = this.BehaviorArgs[i];

                e.Force = this.GameDynamics.PostProcessFinalForce(c.Statics, e.Force);
                this.GameDynamics.CalculateNewStatics(c, e.Force);
            }

            if (this.PositionCleanupDelay != 0 & this.CurrentTime > this.LastPositionCleanup + this.PositionCleanupDelay)
            {
                for (var i = 0; i < this.Creatures.Count; i++)
                {
                    var c = this.Creatures[i];
                    c.Statics.Position = this.CoordinateSystem.VectorToPoint(c.Statics.Position);
                }

                this.LastPositionCleanup = this.CurrentTime;
            }

            this.AddRemoveCreaturesAllowed = true;

            this.GameDynamics.PostFrameOperations();

            this.CurrentTime += this.FrameLength;
        }

        public void AdvanceFrameToTime(double time)
        {
            while (this.CurrentTime + this.FrameLength < time)
            {
                this.AdvanceOneFrame();
            }
            if (this.CurrentTime < time &&
                Math.Abs(this.CurrentTime + this.FrameLength - time) < Math.Abs(this.CurrentTime - time))
            {
                this.AdvanceOneFrame();
            }
        }

        public double CurrentTime
        {
            get;
            private set;
        }

        public double PositionCleanupDelay
        {
            get;
            set;
        }

        #region FramesPerSecond Property
        private double _FramesPerSecond;

        public double FramesPerSecond
        {
            get => this._FramesPerSecond;
            set
            {
                this._FramesPerSecond = value;
                this.FrameLength = 1.0 / value;
            }
        }
        #endregion

        #region FrameLength Property
        private double _FrameLength;

        public double FrameLength
        {
            get => this._FrameLength;
            private set
            {
                this._FrameLength = value;
                this.GameDynamics.SetParentSettings(this, this.CoordinateSystem, value);
            }
        }
        #endregion

        public CoordinateSystem<TVector> CoordinateSystem
        {
            get;
        }

        public GameDynamics<TVector, TBehaviorArgs> GameDynamics
        {
            get;
        }

        private double LastPositionCleanup;
        private readonly List<Creature<TVector, TBehaviorArgs>> Creatures = new List<Creature<TVector, TBehaviorArgs>>();
        private readonly List<TBehaviorArgs> BehaviorArgs = new List<TBehaviorArgs>();

    }
}
