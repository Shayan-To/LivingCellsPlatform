﻿using System;

namespace Ks.LivingCellsPlatform
{
    public abstract class CreatureType<TVector, TBehaviorArgs>
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public abstract void Behavior(TBehaviorArgs e);
        public abstract void BehaviorInitialize(TBehaviorArgs e);

        public abstract bool NeedsSensoryData { get; }

    }

    public class DelegateCreatureType<TVector, TBehaviorArgs> : CreatureType<TVector, TBehaviorArgs>
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public DelegateCreatureType(Action<TBehaviorArgs> behaviorInitializeDelegate, Action<TBehaviorArgs> behaviorDelegate, Func<bool> needsSensoryDataDelegate)
        {
            this.BehaviorInitializeDelegate = behaviorInitializeDelegate;
            this.BehaviorDelegate = behaviorDelegate;
            this.NeedsSensoryDataDelegate = needsSensoryDataDelegate;
        }

        public override void BehaviorInitialize(TBehaviorArgs e)
        {
            this.BehaviorInitializeDelegate.Invoke(e);
        }

        public override void Behavior(TBehaviorArgs e)
        {
            this.BehaviorDelegate.Invoke(e);
        }

        public override bool NeedsSensoryData => this.NeedsSensoryDataDelegate.Invoke();

        private readonly Action<TBehaviorArgs> BehaviorInitializeDelegate;
        private readonly Action<TBehaviorArgs> BehaviorDelegate;
        private readonly Func<bool> NeedsSensoryDataDelegate;

    }
}
