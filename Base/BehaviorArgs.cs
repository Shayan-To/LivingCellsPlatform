﻿using System.Collections.Generic;

namespace Ks.LivingCellsPlatform
{
    public class BehaviorArgs<TVector>
    {

        public virtual void Reset(CreatureStatics<TVector> statics, double currentTime)
        {
            this.LastAppliedForce = this.Force;
            this.Force = default(TVector);
            this.Statics = statics;
            this.CurrentTime = currentTime;
            unchecked
            {
                this.Version += 1;
            }
        }

        public TVector Force
        {
            get;
            set;
        }

        public double CurrentTime
        {
            get;
            private set;
        }

        public TVector LastAppliedForce
        {
            get;
            private set;
        }

        public CreatureStatics<TVector> Statics
        {
            get;
            private set;
        }

        internal int Version
        {
            get;
            private set;
        }

        public StateDictionary State { get; } = new StateDictionary();

    }
}
