﻿using System;

namespace Ks.LivingCellsPlatform
{
    public abstract class GameDynamics<TVector, TBehaviorArgs>
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public GameDynamics()
        {
        }

        /// <returns>The force applied to the first object.</returns>
        public abstract TVector CalculateInteractionForce(CreatureStatics<TVector> c1, CreatureStatics<TVector> c2);
        public abstract TVector CalculateEnvironmentalForce(CreatureStatics<TVector> c);

        public abstract void CalculateSensoryData(Creature<TVector, TBehaviorArgs> creature, TBehaviorArgs e);

        public abstract void PreFrameOperations();
        public abstract void PostFrameOperations();

        public abstract void PostProcessBehaviorArgs(Creature<TVector, TBehaviorArgs> creature, TBehaviorArgs e);
        public abstract TVector PostProcessFinalForce(CreatureStatics<TVector> creature, TVector force);

        public virtual double CalculateMass(CreatureStatics<TVector> statics)
        {
            return Math.PI * statics.Size * statics.Size / 2000;
        }

        public virtual void CalculateNewStatics(Creature<TVector, TBehaviorArgs> creature, TVector force)
        {
            var statics = creature.Statics;
            var m = this.CalculateMass(statics);
            var v1 = statics.Velocity;
            var dv = this.CoordinateSystem.Scale(force, this.FrameLength / m);
            var v2 = this.CoordinateSystem.Add(v1, dv);
            var vbar = this.CoordinateSystem.Add(v1, this.CoordinateSystem.Scale(dv, 0.5));
            var dp = this.CoordinateSystem.Scale(vbar, this.FrameLength);

            creature.Statics.Position = this.CoordinateSystem.Add(statics.Position, dp);
            creature.Statics.Velocity = v2;
        }

        public void SetParentSettings(CreatureStore<TVector, TBehaviorArgs> creatures, CoordinateSystem<TVector> coordinateSystem, double frameLength)
        {
            this.Creatures = creatures;
            this.CoordinateSystem = coordinateSystem;
            this.FrameLength = frameLength;
        }

        public double FrameLength
        {
            get;
            private set;
        }

        public CoordinateSystem<TVector> CoordinateSystem
        {
            get;
            private set;
        }

        public CreatureStore<TVector, TBehaviorArgs> Creatures
        {
            get;
            private set;
        }

    }
}
