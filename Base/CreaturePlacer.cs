﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ks.LivingCellsPlatform
{
    public abstract class CreaturePlacer<TVector, TBehaviorArgs>
        where TBehaviorArgs : BehaviorArgs<TVector>, new()
    {

        public abstract void PlaceCreatures(IEnumerable<CreatureType<TVector, TBehaviorArgs>> creatureTypes);

        public Game<TVector, TBehaviorArgs> Game
        {
            get;
            internal set;
        }

    }
}
