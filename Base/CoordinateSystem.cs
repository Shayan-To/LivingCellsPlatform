﻿using System;
using System.Collections.Generic;
using Avalonia.Media;

namespace Ks.LivingCellsPlatform
{
    /// <typeparam name="TVector">Notice: default(TVector) should be the zero vector.</typeparam>
    public abstract class CoordinateSystem<TVector>
    {

        public abstract TVector NewVector(double length, double angle);
        public abstract TVector Add(TVector v1, TVector v2);
        public abstract TVector Scale(TVector v, double f);
        public abstract double Angle(TVector v);

        public abstract TVector VectorToPoint(TVector v);
        public abstract IEnumerable<TVector> GetStandardForms(TVector point);
        public abstract IEnumerable<(TVector Direction, double Distance)> GetHittingWalls(TVector point, double radius);
        public abstract (double X, double Y) GetCoordinatesForDisplay(TVector point);

        public abstract Geometry GetBoardGeometry();

        public TVector ZeroVector => default(TVector);

        public double Deg(double angle)
        {
            return -angle / 180 * Math.PI;
        }

        public virtual TVector NewPoint(double length, double angle)
        {
            return this.VectorToPoint(this.NewVector(length, angle));
        }

        public virtual TVector ShortestForm(TVector point)
        {
            var l = double.MaxValue;
            var res = this.ZeroVector;

            foreach (var p in this.GetStandardForms(point))
            {
                var t = this.Length(p);
                if (t < l)
                {
                    l = t;
                    res = p;
                }
            }

            return res;
        }

        public virtual TVector Subt(TVector v1, TVector v2)
        {
            return this.Add(v1, this.Negate(v2));
        }

        public virtual TVector Negate(TVector v)
        {
            return this.Scale(v, -1);
        }

        public virtual TVector Normalize(TVector v)
        {
            return this.Scale(v, 1 / this.Length(v));
        }

        public virtual double LengthSquared(TVector v)
        {
            var t = this.Length(v);
            return t * t;
        }

        public virtual double Length(TVector v)
        {
            return Math.Sqrt(this.LengthSquared(v));
        }

        public virtual double DistToOriginSquared(TVector point)
        {
            return this.LengthSquared(this.ShortestForm(point));
        }

        public virtual double DistToOrigin(TVector point)
        {
            return this.Length(this.ShortestForm(point));
        }

        public virtual double Dist(TVector v1, TVector v2)
        {
            return this.DistToOrigin(this.Subt(v1, v2));
        }

        public virtual double DistSquared(TVector v1, TVector v2)
        {
            return this.DistToOriginSquared(this.Subt(v1, v2));
        }

    }
}
