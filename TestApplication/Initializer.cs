﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ks.LivingCellsPlatform.TestApplication
{
    using Vector = CartesianVector;
    using BehaviorArgs = BehaviorArgs<CartesianVector>;

    public static class Initializer
    {

        public static GameHandler<Vector, BehaviorArgs> CreateGameHandler()
        {
            var cs = new LoopThroughCartesianCoordinateSystem(400, 400);
            var gd = new SampleGameDynamics<Vector>();
            var placer = new AtOriginCreaturePlacer<Vector, BehaviorArgs> { CountOfEach = 25 };
            var gh = GameHandler.Create(cs, gd, placer);
            gh.Game.PositionCleanupDelay = 5;
            return gh;
        }

        public static void InitializeCreatures(GameHandler<Vector, BehaviorArgs> gh)
        {
            var rnd = gh.Random;
            var cs = gh.Game.CoordinateSystem;

            var lastForce = gh.NewStateKey<Vector>();

            var creatureType = gh.AddCreatureType(e =>
            {
                lastForce[e.State] = cs.ZeroVector;
            }, e =>
            {
                if (e.CurrentTime % 4 < 2)
                {
                    if (cs.Length(lastForce[e.State]) == 0)
                    {
                        lastForce[e.State] = cs.NewVector(100, cs.Deg(rnd.NextDouble() * 360));
                    }
                    e.Force = lastForce[e.State];
                }
                else
                {
                    lastForce[e.State] = cs.ZeroVector;
                }
            });
        }

    }
}
