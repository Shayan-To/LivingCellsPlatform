﻿using Avalonia;
using Avalonia.Logging.Serilog;
using Ks.LivingCellsPlatform.UI;

namespace Ks.LivingCellsPlatform.TestApplication
{
    public static class Program
    {

        public static void Main(string[] args)
        {
            LivingCellsViewer.StartApp(BuildAvaloniaApp(), () =>
            {
                var gh = Initializer.CreateGameHandler();
                Initializer.InitializeCreatures(gh);
                gh.FinishInitialization();
                return gh.Game;
            });
        }

        public static AppBuilder BuildAvaloniaApp()
        {
            return AppBuilder.Configure<LivingCellsViewerApp>()
                             .UseWin32().UseDirect2D1()
                             //.UsePlatformDetect()
                             .UseReactiveUI()
                             .LogToDebug();
        }

    }
}
